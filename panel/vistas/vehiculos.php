<?php
  include_once("controlador/vehiculos.php");
?>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Vehículos</h4>
		
		<div class="text-right">
			<a href="?op=vehiculo" class="color-b"><b><i class="fa fa-plus-circle"></i> Registrar vehículo</b></a>
		</div>
	</div>

	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>#</th>
						<th>Vehículo</th>
						<th>Modelo</th>
						<th>Año</th>
						<th>Transmisión</th>
						<th>Estatus</th>
						<th>En Stock?</th>
						<th>Acciones</th>
					</tr>
				</thead>

				<tbody>
                    <?php
                      $veh = new Vehiculo();
                      $rm = $veh->fetchAll();
                      $i = 0;
                      while($fm = $rm->fetch_assoc()){
                        $i++;
                    ?>
                      <tr>
                          <td><?php echo $i;?></td>
                          <td><?php echo $fm['nom_veh'];?></td>
                          <td><?php echo $fm['modelo'];?></td>
                          <td><?php echo $fm['ano_veh'];?></td>
                          <td><?php echo $fm['tra_veh'];?></td>
                          <td><?php echo ($fm['est_veh']==1)?'Visible':'No visible';?></td>
                          <td><?php echo ($fm['sto_veh']==1)?'Si':'No';?></td>
                          <td>
                              <a href='?op=vehiculo&id=<?php echo $fm['idv'];?>'><i class='mr-2 fa fa-edit'></i></a>
                              <a href="?op=vehiculo&el=<?php echo $fm['idv'];?>" onclick="return confirm('¿ Está seguro ?');"><i class='mr-2 fa fa-trash'></i></a>
                          </td>
                      </tr>

                    <?php
                      }
                    ?>

				</tbody>
			</table>
		</div>
	</div>
</div>

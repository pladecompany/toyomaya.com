<?php
  include_once("controlador/repuestos.php");
?>

<style>
  table td{
    padding:0px !important;
  }
</style>
<div class="title-box-d">
	<?php if(isset($F)){ ?>
	<h3 class="title-d" id="titulo_modulo">Editar repuesto</h3>
	<?php }else{?>
	<h3 class="title-d" id="titulo_modulo">Nuevo repuesto</h3>
	<?php }?>

</div>
<div class="card shadow mb-4">

	<div class="card-body">
    <div class="text-left">
      <a href="?op=repuestos" class=""><i class='fa fa-arrow-left'></i> Listado de repuestos</a>
    </div>
      <br>
      <?php include_once("mensajes.php");?>
		<form class="form-a" method="POST" action="" enctype="multipart/form-data" id="formulario_noticia">
			<?php if(isset($F)) echo "<input type='hidden' name='idn' value='".$F['id']."'>";?>
			<div class="row">
				<div class="col-sm-12 col-md-2 mb-2 offset-md-1">
					<img id="img1" src="<?php echo (isset($F) && $F['img1'] != null)?$F['img1']:'../static/img/1.jpg';?>" alt="Subir imagen" class="img-cargarVehiculo" title="Imagen 1" />
					<div class="custom-file">
						<input type="file" class="custom-file-input" id="customFile1" onchange="subirImg1(this);" name="img1" <?php if(isset($F) == false) echo 'required';?> >
						<label class="custom-file-label" for="customFile1" style="width:100%"></label>
					</div>
				</div>

				<div class="col-sm-12 col-md-2 mb-2">
					<img id="img2" src="<?php echo (isset($F) && $F['img2'] != null)?$F['img2']:'../static/img/1.jpg';?>" alt="Subir imagen" class="img-cargarVehiculo" title="Imagen 2" />
					<div class="custom-file">
						<input type="file" class="custom-file-input" id="customFile2" onchange="subirImg2(this);" name="img2">
						<label class="custom-file-label" for="customFile2" style="width:100%"></label>
					</div>
				</div>

				<div class="col-sm-12 col-md-2 mb-2">
					<img id="img3" src="<?php echo (isset($F) && $F['img3'] != null)?$F['img3']:'../static/img/1.jpg';?>" alt="Subir imagen" class="img-cargarVehiculo" title="Imagen 3" />
					<div class="custom-file">
						<input type="file" class="custom-file-input" id="customFile3" onchange="subirImg3(this);" name="img3">
						<label class="custom-file-label" for="customFile3" style="width:100%"></label>
					</div>
				</div>

				<div class="col-sm-12 col-md-2 mb-2">
					<img id="img4" src="<?php echo (isset($F) && $F['img4'] != null)?$F['img4']:'../static/img/1.jpg';?>" alt="Subir imagen" class="img-cargarVehiculo" title="Imagen 4" />
					<div class="custom-file">
						<input type="file" class="custom-file-input" id="customFile4" onchange="subirImg4(this);" name="img4">
						<label class="custom-file-label" for="customFile4" style="width:100%"></label>
					</div>
				</div>

				<div class="col-sm-12 col-md-2 mb-2">
					<img id="img5" src="<?php echo (isset($F) && $F['img5'] != null)?$F['img5']:'../static/img/1.jpg';?>" alt="Subir imagen" class="img-cargarVehiculo" title="Imagen 5" />
					<div class="custom-file">
						<input type="file" class="custom-file-input" id="customFile5" onchange="subirImg5(this);" name="img5">
						<label class="custom-file-label" for="customFile5" style="width:100%"></label>
					</div>
				</div>
			</div>

			<div class="row mt-5">
				<div class="col-sm-12 col-md-3 mb-2">
					<div class="form-group">
						<label for="Título">Codígo</label>
						<input type="text" class="form-control form-control-lg form-control-a" placeholder="Escribe el codígo" name="cod" value="<?php echo $F['cod_rep'];?>" required>
					</div>
				</div>
				<div class="col-sm-12 col-md-3 mb-2">
					<div class="form-group">
						<label for="Título">Nombre</label>
						<input type="text" class="form-control form-control-lg form-control-a" placeholder="Escribe el nombre" name="nom" value="<?php echo $F['nom_rep'];?>" required>
					</div>
				</div>

				<div class="col-sm-12 col-md-3 mb-2">
					<div class="form-group">
						<label for="Título">Categoría</label>
						<select name="mod" class="form-control" required>
							<option value="">Seleccione</option>
                        <?php
                          $mod = new Categoria();
                          $r = $mod->fetchAll();
                          while($fm = $r->fetch_assoc()){
                            if(isset($F) && $F['id_categoria'] == $fm['id']){
                        ?>
							<option value="<?php echo $fm['id'];?>" selected><?php echo strtoupper($fm['nom_cat']);?></option>
                        <?php }else{ ?>
							<option value="<?php echo $fm['id'];?>"><?php echo strtoupper($fm['nom_cat']);?></option>
                        <?php
                              }
                          }
                        ?>
						</select>
					</div>
				</div>

				<div class="col-sm-12 col-md-3 mb-2">
					<div class="form-group">
						<label for="Título">Precio</label>
						<input type="text" class="form-control form-control-lg form-control-a" placeholder="Escribe el precio" name="pre" value="<?php echo $F['pre_rep'];?>">
					</div>
				</div>

                <div class="col-md-12 mb-2">
                    <div class="form-group">
                        <label for="Contraseña">Descripción del repuesto</label>
                        <textarea class="form-control form-control-lg form-control-a" placeholder="Escribe la descripción" style="height: 100px;" minlength="10" name="des"><?php if(isset($F)) echo $F['des_rep'];?></textarea>
                    </div>
                </div>



				<div class="col-sm-12 col-md-4 mb-2">
					<div class="form-group">
						<label for="Título">Seleccione el estatus</label>
						<select name="est" class="form-control" required>
							<option value="">Seleccione</option>
							<option value="1" <?php echo ($F['est_rep']==1)?'selected':'';?>>Publicación visible</option>
							<option value="0" <?php echo ($F['est_rep']==0)?'selected':'';?>>Publicación no visible</option>
						</select>
					</div>
				</div>
			</div>
                <hr>
            <div class="row">
				<div class="col-sm-12 col-md-1 "></div>
				<div class="col-sm-12 col-md-10 mb-2 text-center">
                  <label for="Título">Seleccione el modelo de vehículo para este repuesto</label>
                  <a href="#md-modelos" data-toggle="modal" class="btn btn-danger  modal-trigger" id="bt_nueva_noticia"><b><i class="fa fa-search"></i> </b></a>
                  <br>
                  <br>
                  <b>*Rango de años soportados: Para establecer rangos de años soportados utilizar el signo '-' Ejemplo: 2010-2015</b>
                  <br>
                  <b>*Años separados por coma: Para establecer mas de un año soportado, separelos con el signo ',' Ejemplo: 2010, 2015, 2020</b>
                  <br>
                  <b>*Rangos de años y por separado: Para realizar una combinación de los 2 puntos anteriores utilice el signo '-' y ',' Ejemplo: 2000-2005, 2010, 2015-2020</b>
                  <table class="table table-stripped" id="table_table">
                    <thead>
                    <th>Modelo seleccionado</th>
                    <th>Años soportados</th>
                    <th>-</th>
                    </thead>
                    <tbody id="modelos_seleccionados">
                    <?php
                      if(isset($F)){
                        $rep = new Repuesto();
                        $asignados = $rep->getModelosAsignados($_GET['id']);
                        $n=0;
                        while($fa = $asignados->fetch_assoc()){
                          $n++;
                          echo "<tr id='fila_".$fa['id']."'>";
                          echo "<td>".$fa['modelo']."</td>";
                          echo "<td><input type='text' value='".$fa['anos']."' style='width:100%;' name='anos[]' required><input type='hidden' name='seleccionados[]' value='".$fa['id']."'></td>";
                          echo "<td><button type='button' class='btn btn-danger bt_quitar_seleccionado'><i class='fa fa-trash'></i></button></td>";
                          echo "</tr>";
                        }
                      }
                    ?>
                    </tbody>
                  </table>
                </div>
            </div>
			
			<div class="modal-footer">
				<button type="submit" id="bt_modulo" name="<?php echo ((isset($F))?'btc':'btg')?>" class="btn btn-primary"><?php echo ((isset($F))?'Guardar Cambios':'Guardar')?></button>
			</div>
		</form>
	</div>
</div>

<div id="md-modelos" class="modal modalmedium fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="">
					<h5 class="title-d" id="titulo_modulo" style="color:#fff;">Seleccione modelos</h5>
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
              <table class="table table-stripped text-center" style="font-size:12px !important;">
                <thead>
                  <th>#</th>
                  <th>Modelo</th>
                  <th>-</th>
                </thead>
                <tbody>
                  <?php
                    $rep = new Repuesto();
                    $modelos = $rep->getModelosRepuestos();
                    $n =0;
                    while($f = $modelos->fetch_assoc()){
                      $n++;
                      echo "<tr>";
                      echo "<td class='text-center'>".$n."</td>";
                      echo "<td class='tx_modelo'>".$f['modelo']."</td>";
                      echo "<td><input type='checkbox' class='form-control modelos' value='".$f['id']."'></td>";
                      echo "</tr>";
                    }
                  ?>
                </tbody>
              </table>
			</div>
			<div class="modal-footer">
              <button class="btn btn-danger pull-right" id="bt_seleccionar">Seleccionar </button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    var tabla_selecciondados = $("#table_table").DataTable({
				"language": {
					"url": "../static/lib/JTables/Spanish.json"
				}
      
      });

    $("#bt_seleccionar").click(function(){
      //tabla_selecciondados.clear();
      $(".modelos").each(function(){
        var r = $(this).is(':checked');
        if(r){
          var html = "";
          var fila = [];
          fila[0]=$(this).parent().parent().find(".tx_modelo").text();
          fila[1]="<input type='text' placeholder='Ej: 2015, 2014, 2010' style='width:100%;' name='anos[]' required><input type='hidden' name='seleccionados[]' value='"+$(this).val()+"'>";
          fila[2]="<button type='button' class='btn btn-danger bt_quitar_seleccionado'><i class='fa fa-trash'></i></button>";
          if($("#fila_" + $(this).val()).length==0)
          tabla_selecciondados.row.add(fila).draw();
          //$("#modelos_seleccionados").append(html);
          //$('#table_table').DataTable( { } );
        }
      });
      $("#md-modelos").modal("hide");
    });

    $(document).on('click', '.bt_quitar_seleccionado', function(){
      $(this).parent().parent().remove();
    });
  });

	function subirImg1(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#img1')
				.attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
	function subirImg2(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#img2')
				.attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
	function subirImg3(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#img3')
				.attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
	function subirImg4(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#img4')
				.attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
	function subirImg5(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#img5')
				.attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
</script>

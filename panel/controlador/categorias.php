<?php
  include_once("modelo/Categoria.php"); 
  if(isset($_POST) && isset($_POST['btg'])){

    $nom = $_POST['nom_cate'];
    
    if(strlen($nom) == 0){
      $err = "Debe llenar el campo nombre.";
    }

    if(isset($err)){
      echo "<script>window.location ='?op=categorias&err&msj=$err';</script>";
      exit(1);
    }

    $categoria = new Categoria();
    $categoria->data["id"] = "";
    $categoria->data["nom_cate"] = $nom;
    $r = $categoria->save();
    if($r->affected_rows == 1){
      $id = $r->insert_id;
      $err = "¡Registró correctamente!";
      echo "<script>window.location ='?op=categorias&info&msj=$err';</script>";
      exit(1);
    }else{
      $err = "¡Código/Correo ya existe!";
      echo "<script>window.location ='?op=categorias&err&msj=$err';</script>";
      exit(1);
    }

  }else if(isset($_POST) && isset($_POST['btc'])){

    $nom = $_POST['nom_cate'];
    
    if(strlen($nom) == 0){
      $err = "Debe llenar el campo nombre.";
    }

    if(isset($err)){
      echo "<script>window.location ='?op=categorias&err&msj=$err';</script>";
      exit(1);
    }

    $categoria = new Categoria();
    $categoria->data["nom_cat"] = $nom;
    $id = $_POST['idn'];
    $r = $categoria->edit($id);
    if($r==true){
      $err = "¡Información actualizada!";
      echo "<script>window.location ='?op=categorias&info&msj=$err';</script>";
    }else{
      $err = "No se realizó ningún cambio.";
      echo "<script>window.location ='?op=categorias&id=$id&info&msj=$err';</script>";
    }
    exit(1);


  }else if(isset($_GET['id'])){
    $id = $_GET['id'];
    $categoria = new Categoria();
    $r = $categoria->findById($id);

    if($r==false){
      $err = "No existe ningún registro con el ID ($id).";
      echo "<script>window.location ='?op=categorias&err&msj=$err';</script>";
      exit(1);
    }else{
      $F = $r;
    }
  }else if(isset($_GET['el'])){
    $id = $_GET['el'];
    $categoria = new Categoria();
    if($categoria->removeById($id)){
      $err = "¡Eliminado correctamente!";
      echo "<script>window.location ='?op=categorias&info&msj=$err';</script>";
    }else{
      $err = "El registro no puede ser eliminado, tiene información en uso.";
      echo "<script>window.location ='?op=categorias&err&msj=$err';</script>";
    }
    exit(1);
  }

?>

<?php
  include_once("modelo/Admin.php"); 
  include_once("modelo/Conexion.php"); 

  if(isset($_POST) && isset($_POST['btg'])){

    $cor = $_POST['cor'];
    $usu = $_POST['usu'];
    $niv = $_POST['niv'];
    $pas = $_POST['pas'];

    if(strlen($cor) == 0){
      $err = "Debe llenar el campo correo de acceso.";
    }else if(strlen($usu) < 2){
      $err = "El campo nombre y apellido debe tener al menos 2 carácteres";
    }
    if(isset($err)){
      echo "<script>window.location ='?op=admins&err&msj=$err';</script>";
      exit(1);
    }

    $admin = new Admin();
    $admin->data["id"] = "";
    $admin->data["usuario"] = $usu;
    $admin->data["password"] = md5($pas);
    $admin->data["correo"] = $cor;
    $admin->data["acceso"] = $niv;

    $r = $admin->save();
    if($r->affected_rows == 1){
      $id = $r->insert_id;
      
      $err = "¡Registró correctamente!";
      echo "<script>window.location ='?op=admins&info&msj=$err';</script>";
      exit(1);
    }else{
      $err = "¡Código/Correo ya existe!";
      echo "<script>window.location ='?op=admins&err&msj=$err';</script>";
      exit(1);
    }

  }else if(isset($_POST) && isset($_POST['btc'])){

    $idn = $_POST['idn'];
    $cor = $_POST['cor'];
    $usu = $_POST['usu'];
    $pas = $_POST['pas'];
    $niv = $_POST['niv'];

    if(strlen($cor) == 0){
      $err = "Debe llenar el campo correo de acceso.";
    }else if(strlen($usu) < 2){
      $err = "El campo nombre y apellido debe tener al menos 2 carácteres";
    }
    if(isset($err)){
      echo "<script>window.location ='?op=admins&err&msj=$err';</script>";
      exit(1);
    }

    $cliente = new Admin();
    $cliente->data["id"] = $idn;
    $cliente->data["correo"] = $cor;
    $cliente->data["usuario"] = $usu;
    $cliente->data["acceso"] = $niv;
    if(strlen($pas) > 0)
      $cliente->data["password"] = md5($pas);

    $id = $_POST['idn'];
    $r = $cliente->edit($id);
    if($r==true){
      $err = "¡Información actualizada!";
      echo "<script>window.location ='?op=admins&info&msj=$err';</script>";
    }else{
      $err = "No se realizó ningún cambio.";
      echo "<script>window.location ='?op=admins&id=$id&info&msj=$err';</script>";
    }
    exit(1);


  }else if(isset($_GET['id'])){
    $id = $_GET['id'];
    $cliente = new Admin();
    $r = $cliente->findById($id);
    if($r==false){
      $err = "No existe ningún registro con el ID ($id).";
      echo "<script>window.location ='?op=admins&err&msj=$err';</script>";
      exit(1);
    }else{
      $F = $r;
    }
  }else if(isset($_GET['el'])){
    $id = $_GET['el'];
    $cliente = new Admin();
    if($cliente->removeById($id)){
      $err = "¡Eliminado correctamente!";
      echo "<script>window.location ='?op=admins&info&msj=$err';</script>";
    }else{
      $err = "El registro no puede ser eliminado, tiene información en uso.";
      echo "<script>window.location ='?op=admins&err&msj=$err';</script>";
    }
    exit(1);
  }

?>

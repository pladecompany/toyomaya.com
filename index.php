<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
    session_start();
	include_once('ruta.php');
    include_once("configuraciones.php");
	if($_SERVER['SERVER_NAME']!="localhost"){	
      if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === "off") {
          $location = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
          header('HTTP/1.1 301 Moved Permanently');
          header('Location: ' . $location);
          exit;
      }
    }
    setlocale(LC_ALL,"es_ES");
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="title" content="ToyoMaya">
	<meta name="author" content="Plade Company C.A">
	<meta name="keywords" content="Vehículo, carros, comprar, ventas">
	<meta name="description" content="Servicio automotriz con 35 años de experiencia. Venta de vehículos y repuestos originales.">

	<title>ToyoMaya</title>

	<!-- Favicons -->
	<link href="static/img/favicon.ico" rel="icon">
	<link href="static/img/apple-touch-icon.png" rel="apple-touch-icon">

	<!-- Libraries CSS Files -->
	<link rel="stylesheet" href="static/css/open-iconic-bootstrap.min.css">
	<link rel="stylesheet" href="static/css/animate.css">
	<link rel="stylesheet" href="static/css/owl.carousel.min.css">
	<link rel="stylesheet" href="static/css/owl.theme.default.min.css">
	<link rel="stylesheet" href="static/css/aos.css">
	<link rel="stylesheet" href="static/css/flaticon.css">
	<link rel="stylesheet" href="static/css/icomoon.css">
	<link rel="stylesheet" href="static/lib/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" href="static/css/style.css">
	<link rel="stylesheet" href="static/css/estilos.css">
	<link rel="stylesheet" href="static/css/smoothproducts.css">

	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/JTables/css/jquery.dataTables.min.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/JTables/css/dataTables.bootstrap.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/JTables/css/responsive.dataTables.min.css">

	<!-- Main Stylesheet File -->
	<link href="static/css/style.css" rel="stylesheet">
	<link href="static/css/estilos.css" rel="stylesheet">

	<script src="static/js/jquery.min.js"></script>
	<script src="static/js/jquery-migrate-3.0.1.min.js"></script>
    <script src='fullcalendar/packages/moment.js'></script>
    <?php //include_once("analytics.php");?>
</head>


	<body >
		<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light site-navbar-target" id="ftco-navbar">
			<div class="container">
				<a class="navbar-brand" href="index.php"><img src="static/img/logo.jpg" width="40px"> Toyo<span>Maya</span></a>
				<button class="navbar-toggler js-fh5co-nav-toggle fh5co-nav-toggle" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
					<span class="oi oi-menu"></span> Menu
				</button>

				<div class="collapse navbar-collapse" id="ftco-nav">
					<ul class="navbar-nav nav ml-auto">
						<li class="nav-item"><a href="?op=inicio" class="nav-link"><span>Inicio</span></a></li>
						<li class="nav-item"><a href="<?php echo ((isset($_GET['op'])&& $_GET['op']!='inicio')?'index.php':'');?>#servicio" class="nav-link"><span>Servicio</span></a></li>
						<li class="nav-item"><a href="?op=vehiculos" class="nav-link"><span>Vehículos</span></a></li>
						<li class="nav-item"><a href="?op=repuestos" class="nav-link"><span>Repuestos</span></a></li>
						<li class="nav-item"><a href="<?php echo ((isset($_GET['op'])&& $_GET['op']!='inicio')?'index.php':'');?>#testimonios" class="nav-link"><span>Testimonios</span></a></li>
						<li class="nav-item"><a href="?op=noticias" class="nav-link"><span>Noticias</span></a></li>
						<li class="nav-item"><a href="<?php echo ((isset($_GET['op'])&& $_GET['op']!='inicio')?'index.php':'');?>#contacto" class="nav-link"><span>Contacto</span></a></li>
						
						<?php
							if(isset($_SESSION['log']) && $_SESSION['log'] == true){
						}else{
						?>
							<li class="nav-item"><a href="#" class="nav-link" data-toggle="modal" data-target="#md-ingresar"><span>Ingresar</span></a></li>
						<?php }?>

						<?php
							if($promo['est'] == "1"){
						?>
							<li class="nav-item"><a href="#" id="bt_promo" class="btn btn-primary">Promo</a></li>
						<?php } ?>

						<?php
						if(isset($_SESSION['log'])&&$_SESSION['log'] == true){
						?>
							<button onclick="window.location='?op=inicio_log';" type="button" class="btn btn-primary" style="color:#fff;"> <?php echo "Hola ". $_SESSION['nom'];?> </button>
						<?php
						}else{
						?>
							<button id="bt_registro_modal" type="button" class="btn btn-primary" data-toggle="modal" data-target="#md-registro">Regístrate</button>
						<?php }?>
					</ul>
				</div>
			</div>
		</nav>

		<?php
			include_once($ruta);
		?>
		
		<footer class="ftco-footer ftco-section" style="width: 100%;">
			<div class="container">
				<div class="row mb-5">
					<div class="col-md">
						<div class="ftco-footer-widget mb-4">
							<h2 class="ftco-heading-2">Acerca de ToyoMaya</h2>
							<p>Esta organización ofrece a su distinguida clientela servicios y productos con el respaldo de la Marca TOYOTA</p>
							<ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
								<li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
								<li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
								<li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
							</ul>
						</div>
					</div>
					<div class="col-md">
						<div class="ftco-footer-widget mb-4 ml-md-4">
							<ul class="list-unstyled">
								<li><a href="#nosotros"><span class="icon-long-arrow-right mr-2"></span>Nosotros</a></li>
								<li><a href="#md-ingresar" data-toggle="modal" data-target="#md-ingresar"><span class="icon-long-arrow-right mr-2"></span>Iniciar sesión</a></li>
								<li><a href="#md-registro" data-toggle="modal" data-target="#md-registro"><span class="icon-long-arrow-right mr-2"></span>Registro</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md">
						 <div class="ftco-footer-widget mb-4">
							<ul class="list-unstyled">								
								<li><a href="#servicio"><span class="icon-long-arrow-right mr-2"></span>Servicio</a></li>
								<li><a href="?op=repuestos"><span class="icon-long-arrow-right mr-2"></span>Repuestos</a></li>
								<li><a href="?op=vehiculos"><span class="icon-long-arrow-right mr-2"></span>Vehículos</a></li>

							</ul>
						</div>
					</div>
					<div class="col-md">
						<div class="ftco-footer-widget mb-4">
							<div class="block-23 mb-3">
								<ul>
									<li><span class="icon icon-map-marker"></span><span class="text">Av. Constitución cruce con calle 12 Local ToyoMaya nro 249 San José, Maracay Edo. Aragua. Zona Postal 2103, Maracay - Venezuela</span></li>
									<li><a href="#"><span class="icon icon-phone"></span><span class="text">0243-2363061</span></a></li>
									<li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@toyoMayamaracay.com</span></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12 text-center">
						<p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados, desarrollado por <a href="https://pladecompany.com" target="_blank" style="color: #00b0f0;"> Plade Company <img src="static/img/plade.png"></a></p>
					</div>
				</div>
			</div>
		</footer>


    <?php include_once("modales.php");?>

	<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

	<!-- loader 
		<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
	-->
	<script src="static/js/bootstrap.min.js"></script>
	<script src="static/js/jquery.easing.1.3.js"></script>
	<script src="static/js/jquery.waypoints.min.js"></script>
	<script src="static/js/jquery.stellar.min.js"></script>
	<script src="static/js/owl.carousel.min.js"></script>
	<script src="static/js/aos.js"></script>
	<script src="static/js/jquery.animateNumber.min.js"></script>
	<script src="static/js/scrollax.min.js"></script>
	<script src="static/js/main.js"></script>
	<script src="static/js/registro.js"></script>
	
	<script type="text/javascript" src="static/lib/JTables/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="static/lib/JTables/js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="static/lib/JTables/js/dataTables.responsive.min.js"></script>

	<script type="text/javascript" src="static/js/smoothproducts.min.js"></script>
	<script type="text/javascript">

	$(window).load(function() {
		$('.sp-wrap').smoothproducts();
	});
	</script>

	<script>
		$(document).ready(function(){
			$('.table').DataTable( {
				"language": {
					"url": "static/lib/JTables/Spanish.json"
				}
			} );
			$('.table').DataTable( {
				responsive: true
			} );
		} );
	</script>

	<script type="text/javascript">

		$('#myModal').on('shown.bs.modal', function () {
			$('#myInput').trigger('focus')
		})

        <?php
          if(isset($_GET['reg'])){
        ?>
            $("#bt_registro_modal").trigger('click');
        <?php
            if(isset($_GET['msj'])){
        ?>
            $("#cont_msj").show();
            $("#txt_msj").text("** <?php echo $_GET['msj'];?> **");
        <?php
            }
          }

          if($promo['est'] == "1"){ // Si la promocion es visible desde el panel admin entra acá
            // Validamos la session, si existe la sesion no mostramos modal, cuando es primera vez si lo mostramos.
            if( isset($_SESSION['ok']) ){
            }else{
              $_SESSION['ok'] = true;
        ?>
            $('#md-video').modal('show');
            //$('#video_modal').trigger('play');
        <?php
            }
          }
        ?>


      $(document).on('ready', function(){
        $("#bt_promo").on('click', function(){
            $('#md-video').modal('show');
        });

      <?php
        if(isset($_GET['clave'])){
      ?>
          $("#md-ingresar").modal("show");
          $('#msj_login').text('Su contraseña de ingreso se envio a su correo, verifiquela y vuelva ingresar');
      <?php
        }
      ?>
      });

      $(document).on('click', '#bt_reg_log', function(){
        $("#md-ingresar").modal("hide");
        $("#bt_registro_modal").trigger('click');
      });

      <?php
        if(isset($_GET['err']) && !isset($_GET['reg'])){
          ?>
          $("#bt_ingresar").trigger('click');
          var html = "Datos invalidos, desea registrarse ? <input type='button' class='btn btn-success' value='Registrarse' id='bt_reg_log'>";
          $("#msj_login").html(html);
          <?php
        }
      ?>
      <?php
        if(isset($_GET['olvido'])){
          ?>
          $("#md-olvido").modal("show");
          <?php
        }
      ?>


	</script>
</body>
</html>

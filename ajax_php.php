<?php
    header('Content-type: application/json');
    include_once("panel/modelo/Repuesto.php");
    
    $modulo = $_POST['modulo'];
    $tipo =  $_POST['tipo'];
    

    if($modulo == 'repuestos'){
      if($tipo == "solicitudRepuesto"){
        $repuesto = new Repuesto();
        $repuesto->data['id'] = '';
        $repuesto->data['nom'] = $_POST['nom'];;
        $repuesto->data['ape'] = $_POST['ape'];;
        $repuesto->data['tel'] = $_POST['tel'];;
        $repuesto->data['cor'] = $_POST['cor'];;
        $repuesto->data['repuesto'] = $_POST['res'];
        $repuesto->data['fec_reg_sol'] = date('Y-m-d H:i:s');
        $repuesto->data['estatus'] = '0';
        $repuesto->data['fec_pro_sol'] = '';
        $repuesto->data['id_repuesto'] = $_POST['idr'];;
        $r = $repuesto->insertarSolicitud();
        if($r!=false)
          echo json_encode(array("r"=>true, "msj"=>"Su solicitud se envio correctamente, le contactaremos para darle información."));
        else
          echo json_encode(array("r"=>false, "msj"=>"No se pudo procesar su solicitud, intentelo mas tarde."));
      }
    }
?>

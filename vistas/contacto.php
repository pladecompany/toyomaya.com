<section class="ftco-section contact-section ftco-no-pb" id="contacto">
	<div class="container">
		<div class="row justify-content-center mb-5 pb-3">
			<div class="col-md-7 heading-section text-center ftco-animate">
				<span class="subheading">Escríbenos</span>
				<h2 class="mb-1">Contáctanos</h2>
				<p>Estamos a tu disposición para cualquier duda o información que necesites</p>
			</div>
		</div>
		<div class="row d-flex contact-info mb-5">
			<div class="col-md-6 col-lg-3 d-flex ftco-animate">
				<div class="align-self-stretch box p-4 text-center">
					<div class="icon d-flex align-items-center justify-content-center">
						<span class="flaticon-location"></span>
					</div>
					<h3 class="mb-4">Dirección</h3>
					<p>Av. Constitución cruce con calle 12 Local ToyoMaya nro 249 San José, Maracay Edo. Aragua. Zona Postal 2103, Maracay - Venezuela.</p>
				</div>
			</div>
			<div class="col-md-6 col-lg-3 d-flex ftco-animate">
				<div class="align-self-stretch box p-4 text-center">
					<div class="icon d-flex align-items-center justify-content-center">
						<span class="flaticon-network"></span>
					</div>
					<h3 class="mb-4">Número de contacto</h3>
					<p><a href="tel://1234567920">0243-2363061</a></p>
				</div>
			</div>
			<div class="col-md-6 col-lg-3 d-flex ftco-animate">
				<div class="align-self-stretch box p-4 text-center">
					<div class="icon d-flex align-items-center justify-content-center">
						<span class="flaticon-paper-plane"></span>
					</div>
					<h3 class="mb-4">Correo electrónico</h3>
					<p><a href="mailto:info@toyomayamaracay.com">info@toyomayamaracay.com</a></p>
				</div>
			</div>
			<div class="col-md-6 col-lg-3 d-flex ftco-animate">
				<div class="align-self-stretch box p-4 text-center">
					<div class="icon d-flex align-items-center justify-content-center">
						<span class="flaticon-planet-earth"></span>
					</div>
					<h3 class="mb-4">Página web</h3>
					<p><a href="#">www.toyomayamaracay.com</a></p>
				</div>
			</div>
		</div>

		<div class="row no-gutters block-9">
			<div class="col-md-6 order-md-last d-flex">
				<form class="form-a contactForm" action="" method="post" role="form">
					<!--<div id="sendmessage">Tu mensaje será enviado. ¡Gracias!</div>-->
					<div id="errormessage"></div>
					<div class="row">
						<div class="col-md-6 mb-3">
							<div class="form-group">
								<input type="text" name="name" class="form-control" placeholder="Nombre" data-rule="minlen:4" data-msg="Escribe mínimo 3 carácteres">
								<div class="validation"></div>
							</div>
						</div>

						<div class="col-md-6 mb-3">
							<div class="form-group">
								<input name="email" type="email" class="form-control" placeholder="Correo electrónico" data-rule="email" data-msg="Escribe un email válido">
								<div class="validation"></div>
							</div>
						</div>

						<div class="col-md-12 mb-3">
							<div class="form-group">
								<input type="url" name="subject" class="form-control" placeholder="Asunto" data-rule="minlen:4" data-msg="Escribe mínimo 5 carácteres">
								<div class="validation"></div>
							</div>
						</div>
						
						<div class="col-md-12 mb-3">
							<div class="form-group">
								<textarea name="message" class="form-control" name="message" cols="45" rows="8" data-rule="required" data-msg="Escribe mínimo 3 carácteres" placeholder="Mensaje"></textarea>
								<div class="validation"></div>
							</div>
						</div>
						<div class="col-md-12 mb-4">
							<button type="submit" class="btn btn-primary">Enviar mensaje</button>
						</div>
					</div>
				</form>			
			</div>

			<div class="col-md-6 d-flex pr-3">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3926.23592178391!2d-67.58193528513296!3d10.242546992685158!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e803cae2b5aa1dd%3A0xfe3587789a4b3c6b!2sTOYOMAYA!5e0!3m2!1ses!2sve!4v1583445553517!5m2!1ses!2sve" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
			</div>
		</div>
	</div>
</section>

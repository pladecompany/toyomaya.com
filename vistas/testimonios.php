<section class="ftco-section testimony-section" id="testimonios">
	<div class="container">
		<div class="row justify-content-center pb-3">
			<div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
				<h2 class="mb-4">Testimonios</h2>
			</div>
		</div>
		<div class="row ftco-animate justify-content-center">
			<div class="col-md-12">
				<div class="carousel-testimony owl-carousel ftco-owl">
					<div class="item">
						<div class="testimony-wrap text-center py-4 pb-5">
							<div class="user-img" style="background-image: url(static/img/user.png)">
								<span class="quote d-flex align-items-center justify-content-center">
									<i class="icon-quote-left"></i>
								</span>
							</div>
							<div class="text px-4 pb-5">
								<p class="mb-4">Bueno, rápido y eficaz</p>
								<p class="name">LIZ FAJA C. A</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="testimony-wrap text-center py-4 pb-5">
							<div class="user-img" style="background-image: url(static/img/testimonios/2.png)">
								<span class="quote d-flex align-items-center justify-content-center">
									<i class="icon-quote-left"></i>
								</span>
							</div>
							<div class="text px-4 pb-5">
								<p class="mb-4">Cuando se trate de Toyota, Servicio excelente</p>
								<p class="name">Carlos Martinez</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="testimony-wrap text-center py-4 pb-5">
							<div class="user-img" style="background-image: url(static/img/testimonios/3.png)">
								<span class="quote d-flex align-items-center justify-content-center">
									<i class="icon-quote-left"></i>
								</span>
							</div>
							<div class="text px-4 pb-5">
								<p class="mb-4">Distribuidor de Repuestos y accesorios Toyota</p>
								<p class="name">Javier Enrique Ruiz Hernandez</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="testimony-wrap text-center py-4 pb-5">
							<div class="user-img" style="background-image: url(static/img/testimonios/4.png)">
								<span class="quote d-flex align-items-center justify-content-center">
									<i class="icon-quote-left"></i>
								</span>
							</div>
							<div class="text px-4 pb-5">
								<p class="mb-4">Excelente atencion</p>
								<p class="name">Heidy Castillo</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="testimony-wrap text-center py-4 pb-5">
							<div class="user-img" style="background-image: url(static/img/testimonios/5.png)">
								<span class="quote d-flex align-items-center justify-content-center">
									<i class="icon-quote-left"></i>
								</span>
							</div>
							<div class="text px-4 pb-5">
								<p class="mb-4">Buen servicio y buena atencion al cliente..</p>
								<p class="name">Joseph Guevara</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
   include_once("panel/modelo/Vehiculo.php");
   include_once("panel/modelo/Modelo.php");
?>

<section class="ftco-section ftco-project bg-light" id="vehiculos">
	<div class="container px-md-5">
		<div class="row justify-content-center pb-5">
			<div class="col-md-12 heading-section text-center ftco-animate">
				<span class="subheading">Encuentra tu Toyota</span>
				<h2 class="mb-1">Vehículos</h2>
				<p>Variedad en modelos, disponible en nuestro concesionario</p>
			</div>
		</div>

		<div class="row">
		  <div class="col-md-12 col-lg-12">
			  <nav aria-label="breadcrumb" class="breadcrumb-box justify-content-lg-end">
				  <ul class="breadcrumb text-left" style="padding:0px;font-size:12px;">
					  <li class="breadcrumb-item">
						<b>FILTRAR POR: </b>
					  </li>
					  <li class="breadcrumb-item">
						  <a href="?op=vehiculos">TODOS</a>
					  </li>
					  <li class="breadcrumb-item">
						  <a href="?op=vehiculos&sto=1">EN NUESTRO CONCESIONARIO</a>
					  </li>
					  <li class="breadcrumb-item">
						  <a href="?op=vehiculos&imp=0">NACIONALES</a>
					  </li>
					  <li class="breadcrumb-item">
						  <a href="?op=vehiculos&imp=1">IMPORTADOS</a>
					  </li>
					  <?php
						$modelo = new Modelo();
						$rmo = $modelo->fetchAll();
						while($fmo = $rmo->fetch_assoc()){
					  ?>
					  <li class="breadcrumb-item">
						  <a href="?op=vehiculos&cate=<?php echo $fmo['id'];?>"><b><?php echo strtoupper($fmo['modelo']);?></b></a>
					  </li>
					  <?php } ?>
				  </ul>
			  </nav>
		  </div>
		</div>
		
		<div class="row">
		  <?php
			$veh = new Vehiculo();
			if(isset($_GET['sto'])){
			  $rv = $veh->fetchVehiculosActivosExiauto();
			}else if(isset($_GET['imp'])){
			  $idm = $_GET['imp'];
			  $rv = $veh->fetchVehiculosByImportado($idm);
			}else if(isset($_GET['cate'])){
			  $idm = $_GET['cate'];
			  $rv = $veh->fetchVehiculosByModelo($idm);
			}else{
			  $rv = $veh->fetchVehiculosByImportado(0);
			  echo "<div class='col-md-12'><h3>Nacionales</h3></div>";
			  $OK = true;
			}

			while($fv = $rv->fetch_assoc()){
		  ?>
			
			<div class="col-md-4">
				<div class="project">
					<div class="img">
						<img src="<?php echo $fv['img1'];?>" alt="" class="img-b img-fluid img-vehiculo">
						<div class="text px-4">
							<h3>
								<a href="?op=vehiculo-ver&id=<?php echo $fv['id'];?>"><?php echo strtoupper($fv['nom_veh']);?></a>
							</h3>
							<span class="category-b"><?php echo $fv['ano_veh'];?></span>
							<?php if($fv['sto_veh'] ==1){ ?>
							<br>
							<div class="pull-right">
								<span style="color:#fff;">Disponible en nuestro concesionario</span>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		  <?php
			}
			if(isset($OK)){
			  $rv = $veh->fetchVehiculosByImportado(1);
			  echo "<div class='col-md-12'><h3>Importados</h3></div>";

			while($fv = $rv->fetch_assoc()){
		  ?>


			<div class="col-md-4">
				<div class="project">
					<div class="img">
						<img src="<?php echo $fv['img1'];?>" alt="" class="img-b img-fluid img-vehiculo">
						<div class="text px-4">
							<h3>
								<a href="?op=vehiculo-ver&id=<?php echo $fv['id'];?>"><?php echo strtoupper($fv['nom_veh']);?></a>
							</h3>
							<span class="category-b"><?php echo $fv['ano_veh'];?></span>
							<?php if($fv['sto_veh'] ==1){ ?>
							<br>
							<div class="pull-right">
								<span style="color:#fff;">Disponible en nuestro concesionario</span>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>

		  <?php
			}
			}else{
			  if($rv->num_rows == 0){
				echo '<center><b>No hay resultados para el filtro seleccionado</b></center>';
			  }
			}
		  ?>
		</div>
	</div>
</section>

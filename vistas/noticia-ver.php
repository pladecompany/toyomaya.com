<?php
  include_once("panel/modelo/Noticia.php");
  $noti_leer = new Noticia();
  
  $idn = $_GET['id'];
  $NOTI = $noti_leer->findById($idn);
  if($NOTI == false){
    echo "<script>window.location ='index.php';</script>";
    exit(1);
  }
?>

<section class="ftco-section bg-light" id="noticias">
	<div class="container">
		<div class="row justify-content-center mb-5 pb-5">
			<div class="col-md-7 heading-section text-center ftco-animate">
				<h2 class="mb-4">Noticias</h2>
				<p>Tenemos para tí informaciones actuales de Toyota</p>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<img src="<?php echo $NOTI['img'];?>" alt="<?php echo $NOTI['titulo'];?>" class="img-fluid" style="width:100%;">
			</div>
			<div class="col-sm-12">
				<h3 class="noti-titulo"><?php echo $NOTI['titulo'];?></h3>
			</div>

			<div class="col-md-12">
				<h5 class="text-justify color-text-a">
					<?php echo nl2br($NOTI['descripcion']); ?>
				</h5>
			</div>
            <br>

			<div class="col-md-12">
				<h6 class="noti-fecha">Publicado el <?php echo $NOTI['fec_reg_noti'];?></h6>
			</div>
		</div>
	</div>
</section>


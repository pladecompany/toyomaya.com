<section id="home-section" class="hero">
	<h3 class="vr">Bienvenido a ToyoMaya</h3>
	<div class="home-slider js-fullheight owl-carousel">
		<div class="slider-item js-fullheight">
			<div class="overlay"></div>
			<div class="container-fluid p-0">
				<div class="row d-md-flex no-gutters slider-text js-fullheight align-items-center justify-content-end" data-scrollax-parent="true">
					<div class="one-third order-md-last img js-fullheight" style="background-image:url(static/img/7.jpg);">
						<div class="overlay"></div>
					</div>
					<div class="one-forth d-flex js-fullheight align-items-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
						<div class="text">
							<span class="subheading">Vehículos</span>
							<h1 class="mb-1 mt-3">Encuentra tú <span>automóvil</span></h1>
							<p>Variedad de automóviles, selecciona de acuerdo a tu preferencia y comodidad</p>
							
							<p><a href="?op=vehiculos" class="btn btn-primary px-5 py-3 mt-3">Ver más</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="slider-item js-fullheight">
			<div class="overlay"></div>
			<div class="container-fluid p-0">
				<div class="row d-flex no-gutters slider-text js-fullheight align-items-center justify-content-end" data-scrollax-parent="true">
					<div class="one-third order-md-last img js-fullheight" style="background-image:url(static/img/100_5706.jpg);">
						<div class="overlay"></div>
					</div>
					<div class="one-forth d-flex js-fullheight align-items-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
						<div class="text">
							<span class="subheading">REPUESTOS</span>
							<h1 class="mb-1 mt-3">Repuestos para tú <span>Vehículo</span></h1>
							<p>Solicita o pregunta por la disponibilidad del repuesto que necesitas para tú vehículo.</p>
							
							<p><a href="?op=repuestos" class="btn btn-primary px-5 py-3 mt-3">Solicitar / preguntar</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include("nosotros.php"); ?>

<section class="ftco-section-2 img" style="background-image: url(static/img/DSC06974.jpg);">
	<div class="container">
		<div class="row d-md-flex justify-content-end">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<a href="#" class="services-wrap ftco-animate">
							<div class="icon d-flex justify-content-center align-items-center">
								<span class="ion-ios-arrow-back"></span>
								<span class="ion-ios-arrow-forward"></span>
							</div>
							<h2>Servicio</h2>
							<p>Mantenimiento para tu Toyota, solicita tu cita ya.</p>
						</a>
						<a href="#" class="services-wrap ftco-animate">
							<div class="icon d-flex justify-content-center align-items-center">
								<span class="ion-ios-arrow-back"></span>
								<span class="ion-ios-arrow-forward"></span>
							</div>
							<h2>Repuestos</h2>
							<p>Solicita o pregunta acerca de repuestos que necesita tu Toyota, totalmente originales.</p>
						</a>
						<a href="#" class="services-wrap ftco-animate">
							<div class="icon d-flex justify-content-center align-items-center">
								<span class="ion-ios-arrow-back"></span>
								<span class="ion-ios-arrow-forward"></span>
							</div>
							<h2>Vehículos</h2>
							<p>Tenemos vehículos último modelo en nuestro concesionario, puedes ver más aquí.</p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php 
	include("vehiculos-todos.php");

	include("servicios.php");

	include("testimonios.php"); 

	include("noticias.php");

	include("contacto.php");

?>

<?php
  include_once("promo.php");
?>

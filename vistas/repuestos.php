<?php
  include_once("panel/modelo/Repuesto.php");  
  include_once("panel/modelo/Categoria.php");  
  include_once("panel/modelo/ModeloRepuesto.php");  
  $repuesto = new Repuesto();
  $categoria = new Categoria();
  $modeloR = new ModeloRepuesto();

  if(isset($_GET['modelo_ano']) && isset($_GET['ano'])){
	$idm = $_GET['modelo_ano'];
	$ano = $_GET['ano'];

	$rep = $modeloR->findById($idm);
	$resultados = $repuesto->getRepuestosByAno($idm, $ano);
	$titulo = "Repuestos pertenecientes al modelo '".$rep['modelo']."' y el año '".$ano."', resultados (" .$resultados->num_rows .")";

  }else if(isset($_GET['categoria'])){
	$idc = $_GET['categoria'];
	$rep = $categoria->findById($idc);
	$resultados = $repuesto->getRepuestosBYCategoria($idc);
	$titulo = "Repuestos pertenecientes a la categoría '".$rep['nom_cat']."', resultados (" .$resultados->num_rows .")";
  }else if(isset($_GET['modelo'])&&!isset($_GET['txt'])){
	$idm = $_GET['modelo'];
	$rep = $modeloR->findById($idm);
	$resultados = $repuesto->getRepuestosBYModelo($idm);
	$titulo = "Repuestos pertenecientes al modelo  '".$rep['modelo']."', resultados (" .$resultados->num_rows .")";
  }else if(isset($_GET['txt'])&&isset($_GET['modelo'])){
	$txt = $_GET['txt'];
    $idm = $_GET['modelo'];
	$rep = $modeloR->findById($idm);
	$resultados = $repuesto->getRepuestosBYNombreModelo($txt, $idm);
	$titulo = "Repuestos de la busqueda '".$txt."' en el modelo '".$rep['modelo']."', resultados (" .$resultados->num_rows .")";
  }else if(isset($_GET['txt'])){
	$txt = $_GET['txt'];
	$resultados = $repuesto->getRepuestosBYNombre($txt);
	$titulo = "Repuestos de la busqueda '".$txt."', resultados (" .$resultados->num_rows .")";
  }else{
	$titulo = "Ultimos repuestos publicados";
	$resultados = $repuesto->getUltimosPublicados(10);
  }
?>
<section class="ftco-section ftco-project bg-light" id="vehiculos">
	<div class="row m-0 pb-5" style="background-image: url(static/img/banner-toyomaya.jpg); background-size: 100%;padding: 50px;">
		<div class="col-md-12 heading-section text-center ftco-animate">
			<h2 class="mb-4 clr_white">Repuestos</h2>
			<p class="clr_white">Encuentra el repuesto que necesitas en ToyoMaya</p>
			<div class="row">
			  <form method="GET" class="row" style="width:100%;">
				<input type="hidden" name="op" value="repuestos">
                <?php
                  if(isset($_GET['modelo'])){
                ?>
                <input type="hidden" name="modelo" value="<?php echo $_GET['modelo'];?>">
                <?php
                  }
                ?>
				<div class="col-md-4 offset-md-4">
					<div class="form-group">
						<input name="txt" type="search" class="form-control" placeholder="" required style="height: auto !important;">
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group text-left">
						<button type="submit" name="bt_buscar" class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
					</div>
				</div>
			  </form>
			</div>
		</div>
	</div>

	<div class="mt-5 px-md-5">
		<div class="row">
			<div class="col-md-3">
				<div class="card">
					<div class="card-header">
						Modelos
					</div>
					<ul class="list-group list-group-flush" style="text-transform:uppercase;">
						<?php
						  $modelos = $repuesto->getModelosRepuestos();
						  while($mode = $modelos->fetch_assoc()){
							$anos = $repuesto->getAnosModelo($mode['id']);
							sort($anos); 
							echo "<li class='list-group-item mostrar_anos' id='".$mode['id']."'><a href='?op=repuestos&modelo=".$mode['id']."'>".$mode['modelo']."</a>";
							if(count($anos)>0){
							  echo "<a class='pull-right' style='cursor:pointer;font-size:10px;'><b>Años</b> <i class='fa fa-arrow-down'></i></a>";
							  echo "<br>";
							  echo '<ul class="list-group list-group-flush" id="anos_'.$mode['id'].'" style="text-transform:uppercase;display:none;">';
							  for($i=0;$i<count($anos);$i++)
								echo "<li class='list-group-item text-right' style='font-size:10px;'><a href='?op=repuestos&modelo_ano=".$mode['id']."&ano=".$anos[$i]."'>".$anos[$i]."</a>";
							  echo '</ul>';
							}
							echo "</li>";
						  }

						?>
					</ul>
				</div>
				<br>
				<div class="card">
					<div class="card-header">
						Categorías
					</div>
					<ul class="list-group list-group-flush" style='text-transform:uppercase;'>
						<?php
						  $categorias = $categoria->fetchAll();
						  while($cate = $categorias->fetch_assoc()){
							echo "<li class='list-group-item'><a href='?op=repuestos&categoria=".$cate['id']."'>".$cate['nom_cat']."</a></li>";
						  }
						?>
					</ul>
				</div>
			</div>

			<div class="col-md-9">
			  <div class="row">
				<div class="col-md-12">
				  <h6><?php echo $titulo; ?></h6>
				  <hr>
				</div>
			  </div>
				<div class="row">
				  <?php
				  while($data = $resultados->fetch_assoc()){
				  ?>
					<div class="col-md-4 mb-3">
						<a href="?op=repuesto-ver&id=<?php echo $data['id'];?>">
							<div class="card">
								<img class="card-img-top img-repuestos" src="<?php echo $data['img1'];?>" alt="">
								<div class="card-body p-3">
									<h5 class="card-title"><?php echo $data['nom_rep'];?></h5>
									<b class="" style="color:#000;">CÓDIGO: <?php echo $data['cod_rep'];?></b>
									<p class="card-text"><?php echo substr($data['des_rep'], 0, 30);?>. . .</p>
								</div>

								<div class="card-body p-3">
									<a href="?op=repuesto-ver&id=<?php echo $data['id'];?>" class="btn btn-primary">Me interesa</a>
								</div>
							</div>
						</a>
					</div>
				  <?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
  $(document).ready(function(){
	$(document).on('click', '.mostrar_anos', function(){
	  var id = this.id;
	  if($("#anos_"+id).is(":visible"))
		$("#anos_"+id).hide();
	  else
		$("#anos_"+id).show();
	});
  });
</script>

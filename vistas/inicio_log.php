<?php
  if(!isset($_SESSION['log'])){
    session_start();
    session_destroy();
    echo "<script>window.location ='index.php';</script>";
    exit(1);
  }


?>

<div style="padding-top:10em"></div>
<?php include_once("vistas/mensajes.php");?>
<div class="row" style="padding:0px;margin:0px;">
<div class="col-md-9">
	<div class="row">
		<a href="?op=misvehiculos" class="col-xl-4 col-md-4 mb-4">
			<div class="card border-left-danger shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold clr_red text-uppercase mb-1">Mis vehículos</div>
						</div>
						<div class="col-auto">
							<i class="fa fa-car fa-2x text-gray-300"></i>
						</div>
					</div>
				</div>
			</div>
		</a>

		<a href="?op=reclamos" class="col-xl-4 col-md-4 mb-4">
			<div class="card border-left-danger shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold clr_red text-uppercase mb-1">Reclamos y sugerencias</div>
						</div>
						<div class="col-auto">
							<i class="fa fa-envelope fa-2x text-gray-300"></i>
						</div>
					</div>
				</div>
			</div>
		</a>

		<a href="?op=cita" class="col-xl-4 col-md-4 mb-4">
			<div class="card border-left-danger shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold clr_red text-uppercase mb-1">Mis citas</div>
						</div>
						<div class="col-auto">
							<i class="fa fa-list fa-2x text-gray-300"></i>
						</div>
					</div>
				</div>
			</div>
		</a>
		
		<a href="?op=perfil" class="col-xl-4 col-md-4 mb-4">
			<div class="card border-left-danger shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold clr_red text-uppercase mb-1">Perfil</div>
						</div>
						<div class="col-auto">
							<i class="fa fa-user fa-2x text-gray-300"></i>
						</div>
					</div>
				</div>
			</div>
		</a>



		<a href="salir.php" class="col-xl-4 col-md-4 mb-4">
			<div class="card border-left-danger shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold clr_red text-uppercase mb-1">Salir</div>
						</div>
						<div class="col-auto">
							<i class="fa fa-lock fa-2x text-gray-300"></i>
						</div>
					</div>
				</div>
			</div>
		</a>
	</div>
</div>
<div class="col-md-3">
  <div class="row">
    <div class="col-md-10 text-center">
      <?php
        include_once("panel/modelo/Cliente.php");
        $idu = $_SESSION['idu'];
        $usu = new Cliente();
        $FUSU = $usu->findById($idu);
        if($FUSU != false){
        }else{
          echo "<script>window.location ='salir.php';</script>";
        }

      ?>
      <form action="panel/controlador/clientes.php" method="POST"  enctype="multipart/form-data" id="formulario_imagen">
        <input type="file" name="img" style="display:none;" id="bt_img_input">
        <input type="hidden" name="bt_img" style="display:none;" id="">
      </form>
      <img src='<?php echo (($FUSU['img_usu']=="")?"static/img/user.png":$FUSU['img_usu']);?>' class="img-profile rounded-circle img img-circle bt_img" style="width:100px;cursor:pointer;" >
      <hr>
      <div class="text-left" style="font-size:12px;">
        <b>Nombre: </b><?php echo $FUSU['nom_usu']." ".$FUSU['ape_usu'];?>
        <br>
        <b>Télefono: </b><?php echo $FUSU['tel_usu'];?>
        <br>
        <b>Correo:</b> <?php echo $FUSU['cor_usu'];?>
        <div class="text-center"><a href="?op=perfil">Editar información</a></div>
        <hr>
      </div>
    </div>
  </div>
</div>
<script>
  $(Document).ready(function(){
    $(".bt_img").click(function(){
      $("#bt_img_input").trigger('click');
    });

    $("#bt_img_input").change(function(){
      if(confirm("Esta seguro que desea cambiar su foto de perfil?")){
        $("#formulario_imagen").submit();
      }
    });
  });
</script>

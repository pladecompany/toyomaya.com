<?php
  include_once("panel/modelo/Vehiculo.php");
  include_once("panel/modelo/Modelo.php");
  $veh = new Vehiculo();
  
  $id = $_GET['id'];
  $VEH = $veh->findById($id);
  if($VEH == false){
	echo "<script>window.location ='index.php';</script>";
	exit(1);
  }

  $mod = new Modelo();
  $modelo = $mod->findById($VEH['id_modelo']);
  $modelo = $modelo['modelo'];
?>

<section class="ftco-section ftco-project bg-light" id="vehiculos">
	<div class="container px-md-5">
		<div class="row justify-content-center pb-5">
			<div class="col-md-12 heading-section text-center ftco-animate">
				<span class="subheading">Vehículos</span>
				<h3><?php echo strtoupper($VEH['nom_veh']);?></h3>
			</div>
		</div>

		<div class="col-xs-12 col-md-9 offset-md-1 p-0 page">
			<div class="sp-loading"><img src="static/img/sp-loading.gif" alt=""></div>
			<div class="sp-wrap">
				<a href="<?php echo $VEH['img1'];?>">
					<img src="<?php echo $VEH['img1'];?>" alt="" width="100%">
				</a>
				<?php if($VEH['img2'] != null) { ?>
					<a href="<?php echo $VEH['img2'];?>"><img src="<?php echo $VEH['img2'];?>" alt="" width="100%"></a>
				<?php } ?>
				<?php if($VEH['img3'] != null) { ?>
					<a href="<?php echo $VEH['img3'];?>"><img src="<?php echo $VEH['img3'];?>" alt="" width="100%"></a>
				<?php } ?>
				<?php if($VEH['img4'] != null) { ?>
					<a href="<?php echo $VEH['img4'];?>"><img src="<?php echo $VEH['img4'];?>" alt="" width="100%"></a>
				<?php } ?>
				<?php if($VEH['img5'] != null) { ?>
					<a href="<?php echo $VEH['img5'];?>"><img src="<?php echo $VEH['img5'];?>" alt="" width="100%"></a>
				<?php } ?>
			</div>
		</div>
	</div>
</section>


<div id="md-interesa" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Me interesa</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="a consultarmodal-body" style="text-align:center;">
				<h5>Para mas información comunícate con nosotros al:<b> 0243-2363061</b></h5><br>
				
				<h5>Escríbenos <a href="mailto:ventas@toyomayamaracay.com?Subject=Me interesa este vehículo"><b>ventas@toyomayamaracay.com</b></a></h5>
			</div>
		</div>
	</div>
</div>


<section class="ftco-section ftco-no-pb ftco-no-pt ftco-services bg-light" id="nosotros">
	<div class="container">
		<div class="row mb-3">
			<div class="col-sm-12">
				<h3 class="title-d">Precio: <small><?php if($VEH['pre_veh'] == null || $VEH['pre_veh'] == 0) echo 'A consultar';else echo $VEH['pre_veh']." USD";?></small></h3>
				<a href="#md-interesa" class="btn btn-primary" data-toggle="modal" data-target="#md-interesa">Me interesa</a>
			</div>
		</div>

		<div class="row no-gutters">
			<div class="col-md-4 ftco-animate py-5 nav-link-wrap">
				<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					<a class="nav-link px-4 active" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-1" aria-selected="true"><span class="mr-3 flaticon-checklist"></span> Especificaciones</a>

					<a class="nav-link px-4" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-2" aria-selected="false"><span class="mr-3 flaticon-chat"></span> Descripción</a>

					<a class="nav-link px-4" id="v-pills-3-tab" data-toggle="pill" href="#v-pills-3" role="tab" aria-controls="v-pills-3" aria-selected="false"><span class="mr-3 flaticon-car-engine"></span> Flexibilidad</a>

					<a class="nav-link px-4" id="v-pills-4-tab" data-toggle="pill" href="#v-pills-4" role="tab" aria-controls="v-pills-4" aria-selected="false"><span class="mr-3 flaticon-car-insurance"></span> Diseño</a>

					<a class="nav-link px-4" id="v-pills-5-tab" data-toggle="pill" href="#v-pills-5" role="tab" aria-controls="v-pills-5" aria-selected="false"><span class="mr-3 flaticon-car-insurance-1"></span> Seguridad</a>
				</div>
			</div>

			<div class="col-md-8 ftco-animate p-4 p-md-5 d-flex align-items-center">
				<div class="tab-content pl-md-5" id="v-pills-tabContent">
					<div class="tab-pane fade show active py-5" id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-1-tab">
						<span class="icon mb-3 d-block flaticon-checklist"></span>
						<h2 class="mb-4">Especificaciones</h2>

						<div class="summary-list">
								<h5><b>Modelo:</b> <span><?php echo strtoupper($modelo);?></span></h5>

								<h5><b>Transmisión:</b> <span><?php echo strtoupper($VEH['tra_veh']);?></span></h5>

							<?php if($VEH['sto_veh'] == 1) { ?> 
								<h5><b>¿ Disponible en exiauto ?</b> <span><?php echo ($VEH['sto_veh']==1)?'Si':'No';?></span></h5>
							<?php } ?>
								<h5><b>Año:</b> <span><?php echo ($VEH['ano_veh']);?></span></h5>
								<h5><b>Tipo:</b> <span><?php echo ($VEH['imp_veh']==1)?'IMPORTADO':'NACIONAL';?></span></h5>
							 <?php if($VEH['doc'] != null) { ?> 
								<h5><b>Documento detallado </b> <span><a href='<?php echo ($VEH['doc']);?>' style="color:red;" target="__blank">Url</a></span></h5>
							<?php } ?>
						</div>
					</div>

					<div class="tab-pane fade py-5" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-2-tab">
						<span class="icon mb-3 d-block flaticon-chat"></span>
						<h2 class="mb-4">Descripción</h2>
						<p class="text-justify"><?php echo nl2br($VEH['des_veh']);?></p>
					</div>

					<div class="tab-pane fade py-5" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-3-tab">
						<span class="icon mb-3 d-block flaticon-car-engine"></span>
						<h2 class="mb-4">Flexibilidad</h2>
						<p class="text-justify"><?php echo nl2br($VEH['fle_veh']);?></p>
					</div>

					<div class="tab-pane fade py-5" id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-4-tab">
						<span class="icon mb-3 d-block flaticon-car-insurance"></span>
						<h2 class="mb-4">Diseño</h2>
						<p class="text-justify"><?php echo nl2br($VEH['dis_veh']);?></p>
					</div>

					<div class="tab-pane fade py-5" id="v-pills-5" role="tabpanel" aria-labelledby="v-pills-5-tab">
						<span class="icon mb-3 d-block flaticon-car-insurance-1"></span>
						<h2 class="mb-4">Seguridad</h2>
						<p class="text-justify"><?php echo nl2br($VEH['seg_veh']);?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

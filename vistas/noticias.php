<?php 
  include_once("panel/modelo/Noticia.php");
  $notit = new Noticia();
  $rnn = $notit->fetchAllActivasLimit(3);
?>

<section class="ftco-section bg-light pb-0" id="noticias">
	<div class="container">
		<div class="row justify-content-center mb-5 pb-5">
			<div class="col-md-7 heading-section text-center ftco-animate">
				<span class="subheading">Infórmate</span>
				<h2 class="mb-1">Noticias</h2>
				<p>Tenemos para tí informaciones actuales de Toyota</p>
			</div>
		</div>

		<div class="row d-flex">
	        <?php
	          while($ffn = $rnn->fetch_assoc()){
                $mes = date("M", strtotime($ffn['fec_reg_noti']));
                $fec = explode("-", $ffn['fec_reg_noti']); 
	        ?>

			<div class="col-md-4 d-flex ftco-animate">
				<div class="blog-entry justify-content-end">
					<img src="<?php echo $ffn['img'];?>" alt="" class="img-a img-fluid img-noticia block-20">

					<div class="text mt-3 float-right d-block">
						<div class="d-flex align-items-center pt-2 mb-4 topp">
							<div class="one mr-2">
								<span class="day"><?php echo $fec[2];?></span>
							</div>
							<div class="two">
								<span class="yr"><?php echo $fec[0];?></span>
								<span class="mos"><?php echo $mes;?></span>
							</div>
						</div>
						<h3 class="heading">
							<a href="?op=noticia-ver&id=<?php echo $ffn['id'];?>"><?php echo $ffn['titulo'];?></a>
						</h3>
						<div class="d-flex align-items-center mt-4 meta">
							<a href="?op=noticia-ver&id=<?php echo $ffn['id'];?>" class="btn btn-primary">Saber más
							</a>
						</div>
					</div>
				</div>
			</div>
	        <?php
	          }
	        ?>
        </div>
    </div>
</section>

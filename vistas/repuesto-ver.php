<?php
  include_once("panel/modelo/Repuesto.php");
  include_once("panel/modelo/ModeloRepuesto.php");
  $veh = new Repuesto();
  
  $id = $_GET['id'];
  $VEH = $veh->findById($id);
  if($VEH == false){
	echo "<script>window.location ='index.php';</script>";
	exit(1);
  }

  //$mod = new Modelo();
  //$modelo = $mod->findById($VEH['id_modelo']);
  //$modelo = $modelo['modelo'];
?>

<section class="ftco-section ftco-project bg-light" id="vehiculos">
	<div class="container px-md-5">
		<div class="row justify-content-center pb-5">
			<div class="col-md-12 heading-section text-center ftco-animate">
				<span class="subheading">Repuestos</span>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12 col-md-5">
				<div class="page">
					<div class="sp-loading"><img src="static/images/sp-loading.gif" alt=""></div>
						<div class="sp-wrap">
							<a href="<?php echo $VEH['img1'];?>">
								<img src="<?php echo $VEH['img1'];?>" alt="" width="50px">
							</a>

							<?php if($VEH['img2'] != null) { ?>
							<a href="<?php echo $VEH['img2'];?>">
								<img src="<?php echo $VEH['img2'];?>" alt="" width="50px">
							</a>
							<?php }?>

							<?php if($VEH['img3'] != null) { ?>
							<a href="<?php echo $VEH['img3'];?>">
								<img src="<?php echo $VEH['img3'];?>" alt="" width="50px">
							</a>
							<?php }?>

							<?php if($VEH['img4'] != null) { ?>
							<a href="<?php echo $VEH['img4'];?>">
								<img src="<?php echo $VEH['img4'];?>" alt="" width="50px">
							</a>
							<?php }?>

							<?php if($VEH['img5'] != null) { ?>
							<a href="<?php echo $VEH['img5'];?>">
								<img src="<?php echo $VEH['img5'];?>" alt="" width="50px">
							</a>
						<?php }?>
					</div>
				</div>
			</div>

			<div class="col-sm-12 col-md-7">
				<h5><?php echo "#<span title='codígo del repuesto'>".$VEH['cod_rep']."</span> - ".$VEH['nom_rep'];?></h5>
				<p><?php echo nl2br($VEH['des_rep']);?></p>
                <hr>
                <center><b>Modelos y años soportados</b></center>
                  <table class="table table-stripped">
                    <?php
                        $rep = new Repuesto();
                        $asignados = $rep->getModelosAsignados($_GET['id']);
                        $n=0;
                        while($fa = $asignados->fetch_assoc()){
                          $n++;
                          echo "<tr id='fila_".$fa['id']."'>";
                            echo "<td>MODELO: </td>";
                            echo "<td>".$fa['modelo']."</td>";
                            echo "</tr>";
                          echo "<tr>";
                            echo "<td>AÑOS: </td>";
                            echo "<td>".$fa['anos_txt']."</td>";
                          echo "</tr>";
                          echo "<tr><td colspan='2'></td></tr>";
                        }
                    ?>
                  </table>
                <hr>
                  <h6 class='text-right'>PRECIO: <?php echo $orm->monto($VEH['pre_rep']);?> $</h6>
                <hr>
				<a href="#md-solicitud" class="pull-right modal-trigger btn btn-danger" data-toggle="modal" data-target="#md-solicitud">Solicitar éste repuesto</a>
			</div>
		</div>

		<hr>
				<div class="row">
                <div class="col-md-12">
                  <h6>Otras publicaciones</h6>
                  <hr>
                </div>
        <?php
          include_once("panel/modelo/Repuesto.php");  
          include_once("panel/modelo/Categoria.php");  
          include_once("panel/modelo/ModeloRepuesto.php");  
          $repuesto = new Repuesto();
          $categoria = new Categoria();
          $modeloR = new ModeloRepuesto();
          $resultados = $repuesto->getUltimosPublicados(6);
                  while($data = $resultados->fetch_assoc()){
                  ?>
					<div class="col-md-4 mb-3">
						<a href="?op=repuesto-ver&id=<?php echo $data['id'];?>">
							<div class="card">
								<img class="card-img-top img-repuestos" src="<?php echo $data['img1'];?>" alt="">
								<div class="card-body p-3">
									<h5 class="card-title"><?php echo $data['nom_rep'];?></h5>
									<p class="card-text"><?php echo substr($data['des_rep'], 0, 30);?>. . .</p>
								</div>

								<div class="card-body p-3">
									<a href="?op=repuesto-ver&id=<?php echo $data['id'];?>" class="btn btn-primary">Me interesa</a>
								</div>
							</div>
						</a>
					</div>
                  <?php } ?>


				</div>

		<div class="col-sm-12 text-center mt-5">
			<a href="?op=repuestos"><h6>Volver a repuestos</h6></a>
		</div>
	</div>
</section>

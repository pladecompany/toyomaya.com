<section class="ftco-counter img ftco-section ftco-no-pt ftco-no-pb" id="servicio">
	<div class="container">
		<div class="row d-flex">
			<div class="col-md-6 col-lg-5 d-flex">
				<div class="img d-flex align-self-stretch align-items-center" style="background-image:url(static/img/DSC06967.jpg);">
				</div>
			</div>
			<div class="col-md-6 col-lg-7 pl-lg-5 py-5">
				<div class="py-md-5">
					<div class="row justify-content-start pb-3">
						<div class="col-md-12 heading-section ftco-animate">
							<span class="subheading">Servicio ToyoMaya</span>
							<h2 class="mb-4" style="font-size: 34px; text-transform: capitalize;">TU TOYOTA AL DIA</h2>
							<p>El mantenimiento de tu automóvil es importante, en ToyoMaya brindamos el mejor servicio, con grandes profesionales, expertos en el área, acercate a nuestro concesionario o solicita una previa cita desde nuestra web para más comodidad.</p>
						</div>
					</div>
					<div class="counter-wrap ftco-animate d-flex mt-md-3">
						<div class="text p-4 bg-primary">
							<p class="mb-0">
								<span class="number" data-number="34">0</span>
								<span>Años de experiencia</span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
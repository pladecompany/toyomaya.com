<?php
   include_once("panel/modelo/Vehiculo.php");
   include_once("panel/modelo/Modelo.php");
?>
<section class="ftco-section ftco-project bg-light" id="vehiculos">
	<div class="container px-md-5">
		<div class="row justify-content-center pb-5">
			<div class="col-md-12 heading-section text-center ftco-animate">
				<span class="subheading">Vehículos</span>
				<h2 class="mb-4">Vehículos</h2>
				<p>Variedad en modelos, disponible en nuestro concesionario</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 testimonial">
				<div class="carousel-project owl-carousel">
                  <?php
                    $veh = new Vehiculo();
                    $rv = $veh->fetchVehiculosLanding(6);
                    while($fv = $rv->fetch_assoc()){
                  ?>
					<div class="item">
						<div class="project">
							<div class="img">
								<img src="<?php echo $fv['img1'];?>" class="img-fluid img-vehiculo" alt="">
								<div class="text px-4">
									<h3><a href="?op=vehiculo-ver&id=<?php echo $fv['id'];?>"><?php echo $fv['nom_veh'];?></a></h3>
									<span><?php echo $fv['ano_ve'];?></span>
								</div>
							</div>
						</div>
					</div>
                  <?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>

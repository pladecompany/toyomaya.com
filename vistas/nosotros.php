<section class="ftco-section ftco-no-pb ftco-no-pt ftco-services bg-light" id="nosotros">
	<div class="container">
		<div class="row no-gutters">
			<div class="col-md-4 ftco-animate py-5 nav-link-wrap">
				<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					<a class="nav-link px-4 active" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-1" aria-selected="true"><span class="mr-3 flaticon-team"></span> Nosotros</a>

					<a class="nav-link px-4" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-2" aria-selected="false"><span class="mr-3 flaticon-scroll"></span> Reseña histórica</a>

					<a class="nav-link px-4" id="v-pills-3-tab" data-toggle="pill" href="#v-pills-3" role="tab" aria-controls="v-pills-3" aria-selected="false"><span class="mr-3 flaticon-eye"></span> Misión</a>

					<a class="nav-link px-4" id="v-pills-4-tab" data-toggle="pill" href="#v-pills-4" role="tab" aria-controls="v-pills-4" aria-selected="false"><span class="mr-3 flaticon-leadership"></span> Visión</a>
				</div>
			</div>

			<div class="col-md-8 ftco-animate p-4 p-md-5 d-flex align-items-center">
				<div class="tab-content pl-md-5" id="v-pills-tabContent">
					<div class="tab-pane fade show active py-5" id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-1-tab">
						<span class="icon mb-3 d-block flaticon-team"></span>
						<h2 class="mb-4">ToyoMaya</h2>
						<p class="text-justify">Esta organización ofrece a su distinguida clientela servicios y productos con el respaldo de la Marca TOYOTA.</p>
					</div>

					<div class="tab-pane fade py-5" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-2-tab">
						<span class="icon mb-3 d-block flaticon-scroll"></span>
						<h2 class="mb-4">Reseña histórica</h2>
						<p class="text-justify">Esta organización se constituyó el 29 de Octubre del año 1985, según consta en el Registro Mercantil de la Circunscripción Judicial del Estado Aragua en el Tomo 165-A No 34, ofreciendo a su distinguida clientela servicios y productos con el respaldo de la Marca TOYOTA.</p>
					</div>

					<div class="tab-pane fade py-5" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-3-tab">
						<span class="icon mb-3 d-block flaticon-eye"></span>
						<h2 class="mb-4">Misión</h2>
						<p class="text-justify"><b>Toyomaya, C.A.</b> mantiene como premisa esencial prestar un servicio de primera, pues su personal calificado y especializado en vehículos de la marca Toyota junto a los equipos y herramientas de alta tecnología conforman una estructura integral que satisface las necesidades de los clientes. De igual forma, procura ofrecer a nuestros clientes rapidez y calidad en nuestros servicios, optimizando continuamente los procesos, capacitando y proyectando a nuestro personal.</p>
					</div>

					<div class="tab-pane fade py-5" id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-4-tab">
						<span class="icon mb-3 d-block flaticon-leadership"></span>
						<h2 class="mb-4">Visión</h2>
						<p class="text-justify">Ser el concesionario automotor de mayor éxito en ventas de vehículos nuevos, repuestos y dando el servicio a nivel regional buscando siempre la satisfacción total de nuestros clientes, a través de gente inspirada, manteniendo su rentabilidad por encima de los estándares. De igual forma, busca alcanzar el liderazgo como empresa de servicio a corto plazo a través de la utilización de la mano de obra calificada, el esmero por mantener informado y capacitado al personal de los avances tecnológicos y la preocupación por adquirir los equipos y herramientas necesarias y adecuadas para desempeñar un trabajo acorde a las exigencias del cliente.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
  include_once("panel/modelo/Cliente.php");

  if(isset($_POST['btl'])){
    $usu = new Cliente();
    $ced1 = $_POST['ced1'];
    $ced2 = $_POST['ced2'];
    $ced = $ced1."-".$ced2;
    $pass = $_POST['pas'];
    $rlog = $usu->login($ced, $pass);

    if($rlog == false){
      session_start();
      session_destroy();
      $err = "Datos invalidos";
      echo "<script>window.location ='index.php?log&err&msj=$err';</script>";
      exit(1);
    }else{
      session_start();

      $_SESSION['log'] = true;
      $_SESSION['idu']=$rlog['id'];
      $_SESSION['nom']=$rlog['nom_usu'];
      echo "<script>window.location ='index.php?op=inicio_log&log&info&msj=Bienvenido';</script>";
      exit(1);
    }
  }else if(isset($_POST['bto'])){
    $usu = new Cliente();
    $ced1 = $_POST['ced1'];
    $ced2 = $_POST['ced2'];
    $ced = $ced1."-".$ced2;
    $fus = $usu->findById($ced);

    if($fus == false){
      echo "<script>window.location ='index.php?olvido=0&no_aplica';</script>";
    }else{
      $cor = explode("@", $fus['cor_usu']);
      $cor1 = substr($cor[0], 0, 3);
      $txt = $cor1 . "*****" . $cor[1];
      echo "<script>window.location ='index.php?olvido=1&paso=2&txt=$txt&ced=$ced';</script>";

    }
  }else if(isset($_POST['bt_clave'])){
    $ced1 = $_POST['ced1'];
    $ced2 = $_POST['ced2'];
    $ced = $ced1."-".$ced2;
    $cor = $_POST['cor'];
    $usu = new Cliente();
    $fusu = $usu->validarOlvido($ced, $cor);
    if($fusu == false){
      echo "<script>window.location ='index.php?olvido=0&no_aplica';</script>";
      exit(1);
    }else{
        $msj = "Su contraseña de ingreso es: ". $fusu['pas_usu'];
        $asunto = "Recuperar contraseña";
        $rc = $usu->enviarCorreo($fusu["cor_usu"], $msj, $asunto);
        echo "<script>window.location ='index.php?clave=1';</script>";
        exit(1);
    }
  }
?>
